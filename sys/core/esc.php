<?php


	function esc($text,$br = false)
	{ 
		if ($br)$array = range(0, 31); 
		else $array = array_merge(range(0,9), range(11, 19), range(21, 31));
		return str_replace(array_map('chr', $array), NULL, $text);
	}